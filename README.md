#  Booking App


Welcome to the Booking App repository!
This project is developed as part of the front-end internship at Algoriza. The  Booking App is designed to help users find and book hotels. Please read the following information to get started with the project.

## Getting Started


To run the project locally, follow these steps:

1- Clone the repository
```bash
git clone https://gitlab.com/Hossam281/algoriza-internship-fe16.git
```
2- Enter the project folder
```bash
cd booking-app
```
3-Install dependencies 
```bash
npm install
```
3-Run the project
```bash
npm run dev
```
## Project Structure
- **src**: Contains the source code for the  Booking App.
- **assets**: Store assets like images and icons.
- **components**: Reusable Vue components used in the app.
- **views**: Vue components representing different views/pages of the app.
- **routes**: Contains vue-router initialization folder.
- **stores**: Contains Pinia stores and global states/actions for the project.

## API Quota Warning

**Important**: The Booking App uses a third-party API for hotel data, and the API quota has been reduced to only 50 calls per month. Exercise caution to avoid exceeding this limit. If the API quota is exceeded, the app may experience disruptions or fail to fetch data. Please be mindful of the API usage to ensure uninterrupted service.

## Technologies Used
- **Vue.js** 
- **Axios**
- **Vue Router**
- **Vue Datepicker**
- **Pinia Store**
- **Headless UI**
- **Vue Toastification**
- **Crypto-JS**
